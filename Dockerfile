FROM node:14.17.1-alpine

WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm install
COPY . /usr/src/app
ENV PORT 3001
EXPOSE $PORT
CMD [ "npm","run", "pm2" ]
