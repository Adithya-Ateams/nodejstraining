import { Request, Response } from "express";
import User, { UserMap } from "../models/user";
import database from "../database";
import { validationResult } from "express-validator";
import DepartmentManager, { DepartmentManagerMap } from '../models/employee_manager';

export async function assignEmployee(req: Request, res: Response) {
  try {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).send({
          message: "Validation error",
          error: errors.array(),
          data: [],
      });
  }
  req.body.first_name = "aaa....";
  let newUser = req.body as User;
  UserMap(database);
  const checkManager = await User.findOne({where: {id: req.body.manager}})
console.log("checkManager...",checkManager);

  if( checkManager != null){
  const id = Number(req.body.id);
  const result = await User.update({manager: req.body.manager},{where: {id:id}});
  res.status(200).json({
    message: "Success",
    status: true,
    error: [],
    // data: result,
  });
}
} catch (e) {
  return res.status(500).send({
      message: "Failed",
      error: e,
      data: [],
  });
}
}
