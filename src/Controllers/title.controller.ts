import { Request, Response } from 'express';
import Title, { TitleMap } from '../models/title';
import { validationResult } from "express-validator";
import database from '../database';

export async function getTitle(req: Request, res: Response) {
  try{
    TitleMap(database);
    const result = await Title.findAll({ where: {deleteKey: true}});
    res.status(200).json({ users: result });
  } catch (e) {
    return res.status(500).send({
        message: "Failed",
        error: e,
        data: [],
    });
  }
  }

  export async function createTitle(req: Request, res: Response) {
    try{
    let newUser = req.body as Title;
    TitleMap(database);
    const result = await Title.create(newUser);
    newUser = result.dataValues as Title;
    res.status(201).json({ user: newUser });
  } catch (e) {
    return res.status(500).send({
        message: "Failed",
        error: e,
        data: [],
    });
  }
}
export async function deleteTitle(req: Request, res: Response) {
  try {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).send({
          message: "Validation error",
          error: errors.array(),
          data: [],
      });
  }
  TitleMap(database);
  const id = Number(req.query.id);
  const result = await Title.update({deleteKey: false},{where: {id:id}});
  res.status(200).json({ user: result });
} catch (e) {
  return res.status(500).send({
      message: "Failed",
      error: e,
      data: [],
  });
}
}