import { Request, Response } from 'express';
import DepartmentEmployee, { DepartmentEmployeeMap } from '../models/department_employee';
import database from '../database';
import User from '../models/user';

export async function getDepartmentEmployee(req: Request, res: Response) {
  try{
    DepartmentEmployeeMap(database);
    const result = await DepartmentEmployee.findAll({ 
      include: ["employees"] 
      // include: [{
      //   model: User,
      //   required: true
      //  }]
    });
    res.status(200).json({ users: result });
  } catch (e) {
    console.log("errooo....",e);
    
    return res.status(500).send({
        message: "Failed",
        error: e,
        data: [],
    });
  }
  }

  export async function createDepartmentEmpolyee(req: Request, res: Response) {
    try{
    let newUser = req.body as DepartmentEmployee;
    DepartmentEmployeeMap(database);
    const result = await DepartmentEmployee.create(newUser);
    newUser = result.dataValues as DepartmentEmployee;
    res.status(201).json({ user: newUser });
  } catch (e) {
    return res.status(500).send({
        message: "Failed",
        error: e,
        data: [],
    });
  }
}
