import { Request, Response } from 'express';
import Salary, { SalaryMap } from '../models/salary';
import { validationResult } from "express-validator";
import database from '../database';

export async function getSalary(req: Request, res: Response) {
  try{
    SalaryMap(database);
    const result = await Salary.findAll({ where: {deleteKey: true}});
    res.status(200).json({ users: result });
  } catch (e) {
    return res.status(500).send({
        message: "Failed",
        error: e,
        data: [],
    });
  }
  }

  export async function createSalary(req: Request, res: Response) {
    try{
    let newUser = req.body as Salary;
    SalaryMap(database);
    const result = await Salary.create(newUser);
    newUser = result.dataValues as Salary;
    res.status(201).json({ user: newUser });
  } catch (e) {
    return res.status(500).send({
        message: "Failed",
        error: e,
        data: [],
    });
  }
}
export async function deletesalary(req: Request, res: Response) {
  try {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).send({
          message: "Validation error",
          error: errors.array(),
          data: [],
      });
  }
  req.body.first_name = "aaa....";
  SalaryMap(database);
  const id = Number(req.query.id);
  const result = await Salary.update({deleteKey: false},{where: {id:id}});
  res.status(200).json({ user: result });
} catch (e) {
  return res.status(500).send({
      message: "Failed",
      error: e,
      data: [],
  });
}
}