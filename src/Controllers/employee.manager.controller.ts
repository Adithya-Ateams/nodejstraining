import { Request, Response } from 'express';
import DepartmentManager, { DepartmentManagerMap } from '../models/employee_manager';
import database from '../database';

export async function getDepartmentManager(req: Request, res: Response) {
  try{
    DepartmentManagerMap(database);
    const result = await DepartmentManager.findAll();
    res.status(200).json({ users: result });
  } catch (e) {
    return res.status(500).send({
        message: "Failed",
        error: e,
        data: [],
    });
  }
  }

  export async function createDepartmentManager(req: Request, res: Response) {
    try{
    let newUser = req.body as DepartmentManager;
    DepartmentManagerMap(database);
    const result = await DepartmentManager.create(newUser);
    newUser = result.dataValues as DepartmentManager;
    res.status(201).json({ user: newUser });
  } catch (e) {
    return res.status(500).send({
        message: "Failed",
        error: e,
        data: [],
    });
  }
}
