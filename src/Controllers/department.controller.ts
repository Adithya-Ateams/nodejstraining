import { Request, Response } from 'express';
import Department, { DepartmentMap } from '../models/department';
import { validationResult } from "express-validator";
import database from '../database';

export async function getDepartment(req: Request, res: Response) {
  try{
    DepartmentMap(database);
    const result = await Department.findAll({ where: {deleteKey: true}});
    res.status(200).json({ users: result });
  } catch (e) {
    return res.status(500).send({
        message: "Failed",
        error: e,
        data: [],
    });
  }
  }

  export async function createDepartment(req: Request, res: Response) {
    try{
    let newUser = req.body as Department;
    DepartmentMap(database);
    const result = await Department.create(newUser);
    newUser = result.dataValues as Department;
    res.status(201).json({ user: newUser });
  } catch (e) {
    return res.status(500).send({
        message: "Failed",
        error: e,
        data: [],
    });
  }
}
export async function deleteDepartment(req: Request, res: Response) {
  try {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).send({
          message: "Validation error",
          error: errors.array(),
          data: [],
      });
  }
  req.body.first_name = "aaa....";
  DepartmentMap(database);
  const id = Number(req.query.id);
  const result = await Department.update({deleteKey: false},{where: {id:id}});
  res.status(200).json({ user: result });
} catch (e) {
  return res.status(500).send({
      message: "Failed",
      error: e,
      data: [],
  });
}
}