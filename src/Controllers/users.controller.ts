import { Request, Response } from "express";
import User, { UserMap } from "../models/user";
import database from "../database";
import Bcrypt from "bcryptjs";
const salt = Bcrypt.genSaltSync(10);
import { validationResult } from "express-validator";
import DepartmentManager, { DepartmentManagerMap } from '../models/employee_manager';
import DepartmentEmployee, { DepartmentEmployeeMap } from '../models/department_employee';


export async function getUsers(req: Request, res: Response) {
  try{
  UserMap(database);
  const result = await User.findAll({ where: {deleteKey: true},
    attributes: {
      exclude: [
        "password"],
    },
    // include: ["managers"]
    include: [{
      model: DepartmentManager,
      required: true
     }]
  });
  res.status(201).json({
    message: "Success",
    status: true,
    error: [],
    data: result 
  });
} catch (e) {
  console.log("errr...",e);
  
  return res.status(500).send({
      message: "Failed",
      error: e,
      data: [],
  });
}
}
export async function getUsersById(req: Request, res: Response) {
  try{
  console.log("iddd...", req.query.id);
  UserMap(database);
  const id = Number(req.query.id);
  const result = await User.findByPk(id);
  res.status(200).json({ user: result });
} catch (e) {
  return res.status(500).send({
      message: "Failed",
      error: e,
      data: [],
  });
}
}
export async function createUsers(req: Request, res: Response) {
  try {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).send({
          message: "Validation error",
          error: errors.array(),
          data: [],
      });
  }
  req.body.first_name = "aaa....";
  const hash = await Bcrypt.hashSync(req.body.password, salt);
  req.body.password = hash;
  let newUser = req.body as User;
  UserMap(database);
  const result = await User.create(newUser);
  console.log("resultt...",result);
  if(req.body.position == "manager"){
    console.log("inside the if consd....");
    DepartmentManagerMap(database)
    const save = await DepartmentManager.create({
      department_id: req.body.department,
      employee_id: result.dataValues.id,
      from_date: new Date(),
      to_date: req.body.to_date
    })
  }
  else if(req.body.position == "employee"){
    console.log("inside the if consd....");
    DepartmentEmployeeMap(database)
    const save = await DepartmentEmployee.create({
      department_id: req.body.department,
      employee_id: result.dataValues.id,
      from_date: new Date(),
      to_date: req.body.to_date
    })
  }

  newUser = result.dataValues as User;
  res.status(201).json({
    message: "Success",
    status: true,
    error: [],
    data: newUser,
  });
} catch (e) {
  return res.status(500).send({
      message: "Failed",
      error: e,
      data: [],
  });
}
}
export async function deleteUsers(req: Request, res: Response) {
  try {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(400).send({
          message: "Validation error",
          error: errors.array(),
          data: [],
      });
  }
  req.body.first_name = "aaa....";
  UserMap(database);
  const id = Number(req.query.id);
  const result = await User.update({deleteKey: false},{where: {id:id}});
  res.status(200).json({ user: result });
} catch (e) {
  return res.status(500).send({
      message: "Failed",
      error: e,
      data: [],
  });
}
}