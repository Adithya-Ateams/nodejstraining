import { Request, Response } from "express";
import { validationResult } from "express-validator";
import database from "../database";
import User, { UserMap } from "../models/user";
import Bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
const salt = Bcrypt.genSaltSync(10);
const tokenKey = "eyJhbGciOiJIUzM4NCJ9.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJhdGVhbWluZGEiLCJVc2VybmFtZSI6ImVsZGhvIiwiZXhwIjoxNjI1MTE3MjQ4LCJpYXQiOjE2MjUxMTcyNDh9.EIyZqHAqAjw1TD5dUm6Yhz0gfT5xrExUKVO1RStE6uib6JNztXwaU33QNMHH57S1";
export async function login(req: Request, res: Response) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).send({
        message: "Validation error",
        error: errors.array(),
        data: [],
      });
    }
    UserMap(database);
    const result = await User.findOne({
      where: {
        email: req.body.email,
      },
    });
    if (result == null) {
        return res.status(422).send({
            message: "You have entered invalid credentials",
            error: [],
            data: [],
        });
    }
    const userData = result;
    const passwordCheck = Bcrypt.compareSync(
        req.body.password,
        userData.password
    );
    console.log("passwordCheck....",passwordCheck);
    
    if (passwordCheck == true) {
        const token = jwt.sign({ userData }, tokenKey);
        return res.status(200).send({
            message: "Success",
            error: [],
            data: {
                token: `Bearer ${token}`,
                id: result.dataValues.id,
                email: result.dataValues.email,
                position: result.dataValues.position,
            },
        });
    }
  } catch (e) {
    return res.status(500).send({
      message: "Failed",
      error: e,
      data: [],
    });
  }
}
