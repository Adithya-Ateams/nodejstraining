import { Router } from 'express';
import  {getDepartment,createDepartment,deleteDepartment}  from '../Controllers/department.controller';
import { body,query } from 'express-validator';

const router = Router();

// GET - Employee_gender
router.get('/', getDepartment );
                                                                     
// POST - Employee_gender
router.post('/create',body('dept_name').notEmpty(), createDepartment);

// DELETE - users
router.delete('/delete',query('id').notEmpty(), deleteDepartment);

export default router;
