import { Router } from 'express';
import  {getDepartmentManager,createDepartmentManager}  from '../Controllers/employee.manager.controller';

const router = Router();

// GET - Employee_gender
router.get('/', getDepartmentManager );
                                                                     
// POST - Employee_gender
router.post('/', createDepartmentManager);

export default router;
