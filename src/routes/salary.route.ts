import { Router } from 'express';
import  {getSalary,createSalary,deletesalary}  from '../Controllers/salary.controller';
import { body,query } from 'express-validator';

const router = Router();

// GET - salary
router.get('/', getSalary );
                                                                     
// POST - Employee_gender
router.post('/create',body('employee_id').notEmpty(),body('amount').notEmpty(), createSalary);

// DELETE - users
router.delete('/delete',query('id').notEmpty(), deletesalary);

export default router;
