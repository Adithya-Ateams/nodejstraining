import { Router } from 'express';
import  {getUsers,getUsersById,createUsers,deleteUsers}  from '../Controllers/users.controller';
import { body,query } from 'express-validator';
import * as authMiddleware from "../middleware/auth.middeleware";
const router = Router();

// GET - users
router.get('/',authMiddleware.authMiddleware, getUsers );
                                                                     
// GET - users/:id
router.get('/id', getUsersById);

// POST - users
router.post('/create',body('first_name').notEmpty(),body('password').isLength({ min: 5 }),authMiddleware.authMiddleware, createUsers);

// DELETE - users
router.delete('/delete',query('id').notEmpty(), deleteUsers);

export default router;
