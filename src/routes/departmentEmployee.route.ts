import { Router } from 'express';
import  {getDepartmentEmployee,createDepartmentEmpolyee}  from '../Controllers/department_employee.controller';

const router = Router();

// GET - Employee_gender
router.get('/', getDepartmentEmployee );
                                                                     
// POST - Employee_gender
router.post('/create', createDepartmentEmpolyee);

export default router;
