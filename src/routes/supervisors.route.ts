import { Router } from 'express';
import  {assignEmployee}  from '../Controllers/supervisors.controller';
import { body,query } from 'express-validator';
import * as authMiddleware from "../middleware/auth.middeleware";
const router = Router();

// PUT - 
router.put('/assignManager', assignEmployee);


export default router;
