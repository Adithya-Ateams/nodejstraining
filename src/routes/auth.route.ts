import { Router } from 'express';
import  {login}  from '../Controllers/auth.controller';
import { body,query } from 'express-validator';

const router = Router();
                                                                     
// POST - Employee_gender
router.post('/login',body('email').notEmpty(),body('password').notEmpty(), login);


export default router;
