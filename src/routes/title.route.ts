import { Router } from 'express';
import  {getTitle,createTitle,deleteTitle}  from '../Controllers/title.controller';
import { body,query } from 'express-validator';

const router = Router();

// GET -
router.get('/', getTitle );
                                                                     
// POST - 
router.post('/create',body('employee_id').notEmpty(),body('amount').notEmpty(), createTitle);

// DELETE - 
router.delete('/delete',query('id').notEmpty(), deleteTitle);

export default router;
