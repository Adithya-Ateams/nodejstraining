import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';

import { Request, Response } from 'express';
import { port } from './config';
import usersRoutes from './routes/users.routes';
import employee_managerRoutes from './routes/employee_manager.routes';
import departmentRoutes from './routes/depatment.routes';
import departmentEmployeeRoute from './routes/departmentEmployee.route';
import salaryRoute from './routes/salary.route';
import titleRoute from './routes/title.route';
import authRoute from './routes/auth.route';
import supervisorRoute from './routes/supervisors.route';

const swaggerUi =require('swagger-ui-express');
const YAML =require('yamljs');
const swaggerDocument = YAML.load('./swagger.yaml');
const cors = require('cors');
console.log("port....",port)
const app = express();
//cors

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get('/', async (req: Request, res: Response) => {
  res.status(200).json({
    message: 'Hello World'
  });
});
app.use('/users', usersRoutes);
app.use('/employeeManager',employee_managerRoutes);
app.use('/department',departmentRoutes);
app.use('/departmentEmployee',departmentEmployeeRoute);
app.use('/salary',salaryRoute);
app.use('/title', titleRoute);
app.use('/auth',authRoute);
app.use('/supervisor',supervisorRoute);

const server = http.createServer(app);
server.listen(3000, () => {
  console.log(`API started at http://localhost:${port}`);
});
