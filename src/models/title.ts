import { Model, Sequelize, DataTypes } from 'sequelize';

export default class Title extends Model {
  public id?: number;
  public employee_id?: number;
  public amount?: number;
  public deleteKey?: boolean;
  public from_date?: Date;
  public to_date?: Date;
}

export const TitleMap = (sequelize: Sequelize) => {
  Title.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    employee_id: {
      type: DataTypes.INTEGER,
      references:{
        model:'employee',
        key:'id'
    },
  },
  title: {
    type: DataTypes.STRING(255),
    allowNull:false,
    validate:{
      notNull:{
        msg: "Please enter first name"
      }
    }
  },
    deleteKey:{
      type: DataTypes.BOOLEAN,
      defaultValue: true // active user
    },
    from_date: {
      type: DataTypes.DATE,
      // allowNull:false,
    },
  to_date: {
      type: DataTypes.DATE,
      // allowNull:false,
    },
  }, {
    sequelize,
    tableName: 'title',
    timestamps: false
  });
  Title.sync();
}
