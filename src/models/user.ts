import { Model, Sequelize, DataTypes } from 'sequelize';
import departmentEmployee from './department_employee';
export default class User extends Model {
  public id?: number;
  public first_name?: string;
  public last_name?: string;
  public birthdate?: Date;
  public gender?: string;
  public hire_date?: Date;
  public email?: string;
  public deleteKey?: boolean;
  public manager?: number;
  static associate(models: any) {
    // define association here
    User.belongsTo(models.departmentManager, {
      foreignKey: 'employee_id',
      as: 'Managers'
  })
  }
}

export const UserMap = (sequelize: Sequelize) => {
  User.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    first_name: {
      type: DataTypes.STRING(255),
      allowNull:false,
      validate:{
        notNull:{
          msg: "Please enter first name"
        }
      }
    },
    last_name: {
      type: DataTypes.STRING(255),
      allowNull:false,
      validate:{
        notNull:{
          msg: "Please enter first name"
        }
      }
    },
    birthdate: {
      type: DataTypes.DATEONLY,
      allowNull:false,
      validate:{
        notNull:{
          msg: "Please enter birthday"
        }
      }
    },
    gender: {
      type: DataTypes.STRING(100),
      allowNull: true,
      validate:{
        isIn: [['F', 'M']],
      }
    },
    hire_date: {
      type: DataTypes.DATE
    },
    position: {
      type: DataTypes.STRING(255)
    },
    email: {
      type: DataTypes.STRING(225),
      allowNull:false,
      unique: true,
      validate:{
          isEmail : true
      },     
  },
  manager:{
    type: DataTypes.INTEGER
  },
  deleteKey:{
    type: DataTypes.BOOLEAN,
    defaultValue: true // active user
  },
  password: {
    type: DataTypes.TEXT
  },
  
  }, {
    sequelize,
    tableName: 'employee',
    timestamps: false,  
  });
  User.sync({alter: true});
}
// User.hasMany(departmentEmployee);
// departmentEmployee.belongsTo(User,{foreignKey: "employee_id"});
