import { Model, Sequelize, DataTypes } from 'sequelize';

export default class Department extends Model {
  public id?: number;
  public dept_name?: string;
  public deleteKey?: boolean;
}

export const DepartmentMap = (sequelize: Sequelize) => {
    Department.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    dept_name: {
      type: DataTypes.STRING(255)
    },
    deleteKey:{
      type: DataTypes.BOOLEAN,
      defaultValue: true // active user
    },
  
  }, {
    sequelize,
    tableName: 'department',
    timestamps: false
  });
  Department.sync();
}
