import { Model, Sequelize, DataTypes } from 'sequelize';

export default class DepartmentManager extends Model {
  public id?: number;
  public title?: string;
}

export const DepartmentManagerMap = (sequelize: Sequelize) => {
  DepartmentManager.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    department_id: {
      type: DataTypes.INTEGER,
      references:{
          model:'department',
          key:'id'
      }
    },
    employee_id: {
        type: DataTypes.INTEGER,
        references:{
          model:'employee',
          key:'id'
      }
      },
    from_date: {
        type: DataTypes.DATE,
        // allowNull:false,
      },
    to_date: {
        type: DataTypes.DATE,
        // allowNull:false,
      },
      deleteKey:{
        type: DataTypes.BOOLEAN,
        defaultValue: true // active user
      },
  }, {
    sequelize,
    tableName: 'departmentManager',
    timestamps: false
  });
  DepartmentManager.sync();
}
