import { Model, Sequelize, DataTypes } from 'sequelize';
import User from './department_employee';
export default class DepartmentEmployee extends Model {
  public id?: number;
  public employee_id?: string;
  public department_id?: string;
  public from_date?: Date;
  public to_date?: Date;
  public deleteKey?: boolean;
  // static associate: (models: any) => void;
    static associate(models: any) {
    // define association here
    DepartmentEmployee.belongsTo(models.User, {
      foreignKey: 'employee_id',
      targetKey: 'id',
      as: 'employees'
    })
  }

  //  DepartmentEmployee.associate = function(models:any) {
  //   DepartmentEmployee.hasOne(models.Employee, {
  //     foreignKey: 'id',
  //     as: 'employee',
  //   });
  // };
}

export const DepartmentEmployeeMap = (sequelize: Sequelize) => {
  DepartmentEmployee.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    department_id: {
      type: DataTypes.INTEGER,
      references:{
          model:'department',
          key:'id'
      }
    },
    employee_id: {
        type: DataTypes.INTEGER,
        references:{
          model: User,
          key:'id'
      }
      },
    from_date: {
        type: DataTypes.DATE,
        // allowNull:false,
      },
    to_date: {
        type: DataTypes.DATE,
        // allowNull:false,
      },
      deleteKey:{
        type: DataTypes.BOOLEAN,
        defaultValue: true // active user
      },
  }, {
    sequelize,
    tableName: 'departmentEmployee',
    timestamps: false,
  });
  // DepartmentEmployee.belongsTo(User, { foreignKey: 'employee_id' });
 
  //  DepartmentEmployee.associate = function(models:any) {
  //   DepartmentEmployee.belongsTo(models.user, {
  //     foreignKey: 'id',
  //     as: 'employees',
  //   });
  // };
  DepartmentEmployee.sync();
}
