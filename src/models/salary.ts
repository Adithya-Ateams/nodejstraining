import { Model, Sequelize, DataTypes } from 'sequelize';

export default class Salary extends Model {
  public id?: number;
  public employee_id?: number;
  public amount?: number;
  public deleteKey?: boolean;
  public from_date?: Date;
  public to_date?: Date;
}

export const SalaryMap = (sequelize: Sequelize) => {
  Salary.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    employee_id: {
      type: DataTypes.INTEGER,
      references:{
        model:'employee',
        key:'id'
    },
  },
  amount: {
    type: DataTypes.INTEGER,
    allowNull:false,
    validate:{
      notNull:{
        msg: "Please enter the amount"
      }
    }
  },
    deleteKey:{
      type: DataTypes.BOOLEAN,
      defaultValue: true // active user
    },
    from_date: {
      type: DataTypes.DATE,
      // allowNull:false,
    },
  to_date: {
      type: DataTypes.DATE,
      // allowNull:false,
    },
  }, {
    sequelize,
    tableName: 'salary',
    timestamps: false
  });
  Salary.sync();
}
