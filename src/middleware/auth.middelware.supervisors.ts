import jwt from "jsonwebtoken";
import express,{NextFunction} from 'express';
import User, { UserMap } from "../models/user";
import database from "../database";
const tokenKey = "eyJhbGciOiJIUzM4NCJ9.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJhdGVhbWluZGEiLCJVc2VybmFtZSI6ImVsZGhvIiwiZXhwIjoxNjI1MTE3MjQ4LCJpYXQiOjE2MjUxMTcyNDh9.EIyZqHAqAjw1TD5dUm6Yhz0gfT5xrExUKVO1RStE6uib6JNztXwaU33QNMHH57S1";


export const authMiddleware = async(req: any, res:any, next:NextFunction) => {
    try {
        const header = req.headers['authorization'];
        if (typeof header !== 'undefined') {
            const bearer = header.split(' ');
            const token = bearer[1];
            req.token = token;
            const decodedToken = jwt.verify(token, tokenKey);
            if (!decodedToken) {
                return res.status(401).send({
                    message: "Unauthorised",
                    error: [],
                    data: []
                })
            }
            console.log("decodedToken........",decodedToken);
            const temp = (decodedToken as any).userData.id
            console.log("temp...",temp);
            UserMap(database);
            const getUsers = await User.findOne({
                where: {
                    id: temp,
                    email: (decodedToken as any).userData.email,
                    position: "supervisor"
                }
            })
            console.log("getuserss..",getUsers);
            
            if (getUsers.length != 0) {
                req.user = (decodedToken as any).userData
                console.log("=====valid user====")
                next();
            } else {
                return res.status(404).send({
                    message: "Invalid token",
                    error: [],
                    data: []
                })
            }
        } else {
            return res.status(400).send({
                message: "Invalid Header",
                error: [],
                data: []
            })
        }
    } catch (e) {
        return res.status(500).send({
            message: "Failed",
            error: e,
            data: []
        })
    }
}